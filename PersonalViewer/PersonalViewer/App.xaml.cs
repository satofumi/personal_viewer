﻿using PersonalViewer.Control;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace PersonalViewer
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            const string uniqueName = Constant.ApplicationName;
            bool created;
            using (var mutex = new System.Threading.Mutex(true, uniqueName, out created))
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

                if (!created)
                {
                    string message = "Application already running.";
                    MessageBox.Show(message, "Critical", MessageBoxButton.OK, MessageBoxImage.Stop);
                    Current.Shutdown();
                }
                else
                {
                    App app = new App();
                    app.InitializeComponent();
                    app.Run();
                }
            }
        }

        /// <summary>
        /// UIスレッド以外の未処理例外スロー時のイベントハンドラ
        /// </summary>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // !!! e.ExceptionObject.ToString()
            Current.Shutdown();
        }
    }
}
