﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer.Control
{
    public static class Constant
    {
        public const string ApplicationName = "PersonalViewer";
        public static readonly string DataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), ApplicationName);
        public static readonly string ProjectFilesFile = Path.Combine(DataFolder, "projects.txt");
        public static readonly string DefaultProjectName = "default";

        public static readonly string TargetFilesFile = "files.txt";
        public static readonly string RemovedFilesFile = "removed_files.txt";
        public static readonly string ThumbnailsFolder = "Thumbnails";
        public static readonly string ThumbnailsFile = "thumbnails.txt";
        public static readonly string FileTagsFolder = "Tags";
        public static readonly string BooksFolder = "Books";
        public static readonly string BooksFile = Path.Combine(BooksFolder, "books.txt");
        public static readonly string BookTagsFolder = Path.Combine(BooksFolder, "BookTags");
        public static readonly string PlayerSettingFolder = "Players";

        public static readonly string BackupExtension = ".bak";
    }
}
