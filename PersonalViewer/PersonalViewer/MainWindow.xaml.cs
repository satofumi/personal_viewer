﻿using Microsoft.WindowsAPICodePack.Dialogs;
using Microsoft.WindowsAPICodePack.Shell;
using PersonalViewer.Control;
using PersonalViewer.Model;
using PersonalViewer.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace PersonalViewer
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand TagFocusCommand = new RoutedCommand();

        private const int LongTimeout = 1000;

        private ProjectManager projects;
        private TargetFilesManager targetFiles;
        private FileTagsManager fileTags;
        private BooksManager books;
        private BookTagsManager bookTags;
        private PlayerSettings playerSettings;
        private ObservableCollection<TargetFile> resultData;


        public MainWindow()
        {
            InitializeComponent();
            Title = "PersonalViewr " + VersionInformation.VersionText;
            LoadWindowRect();
            TreeViewWidth.Width = new GridLength(Properties.Settings.Default.SplitterPosition, GridUnitType.Pixel);
            QueryTextBox.Focus();

            TagFocusCommand.InputGestures.Add(new KeyGesture(Key.T, ModifierKeys.Control));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            resultData = new ObservableCollection<TargetFile>();
            ResultListView.DataContext = resultData;
            ResultIconsView.DataContext = resultData;

            LoadTargetFiles();
            BindEvents();

            SetIconsViewVisible(Properties.Settings.Default.IconsView);
        }

        private void BindEvents()
        {
            TagEditView.AddTag += TagEditView_AddTag;
            FolderTreeView.FolderSelected += FolderTreeView_FolderSelected;
        }

        private void AddProject(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return;
            }
            projects.ProjectNames.Add(name);
            projects.ProjectName = name;
            SetDefaultProject(name);
        }

        private void FolderTreeView_FolderSelected(object sender, FolderTreeView.SelectedFolderEventArgs e)
        {
            QueryTextBox.Text = ":folder:" + e.FolderPath;
        }

        private void LoadWindowRect()
        {
            Top = Properties.Settings.Default.Top;
            Left = Properties.Settings.Default.Left;
            Height = Properties.Settings.Default.Height;
            Width = Properties.Settings.Default.Width;

            if (Properties.Settings.Default.Maximized)
            {
                WindowState = WindowState.Maximized;
            }
        }

        private void SaveWindowRect()
        {
            if (WindowState == WindowState.Maximized)
            {
                Properties.Settings.Default.Top = RestoreBounds.Top;
                Properties.Settings.Default.Left = RestoreBounds.Left;
                Properties.Settings.Default.Height = RestoreBounds.Height;
                Properties.Settings.Default.Width = RestoreBounds.Width;
                Properties.Settings.Default.Maximized = true;
            }
            else
            {
                Properties.Settings.Default.Top = Top;
                Properties.Settings.Default.Left = Left;
                Properties.Settings.Default.Height = Height;
                Properties.Settings.Default.Width = Width;
                Properties.Settings.Default.Maximized = false;
            }

            Properties.Settings.Default.Save();
        }

        private void LoadTargetFiles()
        {
            projects = new ProjectManager();
            targetFiles = new TargetFilesManager();
            fileTags = new FileTagsManager();
            books = new BooksManager();
            bookTags = new BookTagsManager();
            playerSettings = new PlayerSettings();

            Directory.CreateDirectory(Constant.DataFolder);
            projects.ProjectName = string.IsNullOrWhiteSpace(Properties.Settings.Default.Project) ? Constant.DefaultProjectName : Properties.Settings.Default.Project;

            LoadProject();
        }

        private void LoadProject()
        {
            projects.LoadProjects(Constant.ProjectFilesFile);
            if (!projects.ProjectNames.Contains(projects.ProjectName))
            {
                if (projects.ProjectNames.Count == 0)
                {
                    AddProject(Constant.DefaultProjectName);
                }
                else
                {
                    projects.ProjectName = projects.ProjectNames.First();
                }
            }
            projects.CreateFolders();
            projects.LoadData(targetFiles, FolderTreeView, fileTags, books, bookTags, playerSettings);

            SetProjectComboBox();
        }

        private void SetProjectComboBox()
        {
            ProjectComboBox.Items.Clear();
            foreach (var name in projects.ProjectNames)
            {
                ProjectComboBox.Items.Add(name);
            }
            ProjectComboBox.SelectedItem = projects.ProjectName;
        }

        private void TagEditView_AddTag(object sender, View.TagEditView.AddTagEventArgs e)
        {
            var tags = e.Tag.Trim().Split(new char[] { ' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);

            var files = new HashSet<string>();
            FindTaggedFiles(tags, files, ResultListView.SelectedItems);
            FindTaggedFiles(tags, files, ResultIconsView.SelectedItems);
        }

        private void FindTaggedFiles(string[] tags, HashSet<string> files, System.Collections.IList items)
        {
            var indexes = new List<int>(0);

            // 選択されているファイルを取得する。
            foreach (var item in items)
            {
                var targetFile = item as TargetFile;
                files.Add(targetFile.Path);

                int index = ResultListView.Items.IndexOf(item);
                indexes.Add(index);
            }
            foreach (var tag in tags)
            {
                fileTags.AddTag(projects.TagsFolder, tag, files);

                foreach (var path in files)
                {
                    targetFiles.AddTag(path, tag);
                }
            }

            foreach (int index in indexes)
            {
                var data = new TargetFile(resultData[index]);
                data.Tags = CsvTags(data.Path);
                resultData[index] = data;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Properties.Settings.Default.IconsView = (ResultIconsView.Visibility == Visibility.Visible) ? true : false;

            SaveWindowRect();
        }

        private void QueryTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ShowResult();
        }

        private void QueryTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                QueryTextBox.Text = "";
                ShowResult();
            }
        }

        private void ShowResult()
        {
            resultData.Clear();

            var keyword = QueryTextBox.Text;
            if (string.IsNullOrWhiteSpace(keyword))
            {
                ResultListView.ScrollIntoView(null);
                return;
            }

            HashSet<string> tagResult;
            List<string> pathResult;
            string folderCommand = ":folder:";
            if (keyword.IndexOf(folderCommand) == 0)
            {
                tagResult = new HashSet<string>();
                pathResult = targetFiles.FolderResult(keyword.Substring(folderCommand.Length));
            }
            else
            {
                tagResult = fileTags.Result(keyword);
                pathResult = targetFiles.Result(keyword);
            }

            var result = tagResult.Union(pathResult);
            foreach (var path in result)
            {
                var data = new TargetFile(path);
                data.Tags = CsvTags(path);
                resultData.Add(data);
            }
        }

        private void ReshowResult()
        {
            var keyword = QueryTextBox.Text;
            QueryTextBox.Text = "";
            QueryTextBox.Text = keyword;
            ShowResult();
        }

        private string CsvTags(string path)
        {
            var tags = targetFiles.Tags(path);
            return string.Join(", ", tags);
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ((ListViewItem)sender).Content as TargetFile;
            if (item == null)
            {
                return;
            }
            var path = item.Path;

            if (!File.Exists(path))
            {
                return;
            }

            if (playerSettings.IsDefaultProgram(path))
            {
                System.Diagnostics.Process.Start(item.Path);
            }
            else
            {
                System.Diagnostics.ProcessStartInfo process = new System.Diagnostics.ProcessStartInfo();
                process.FileName = "C:\\Program Files\\SMPlayer\\smplayer.exe";
                process.Arguments = string.Format("\"{0}\"", path);
                System.Diagnostics.Process.Start(process);
            }
        }

        private void AddNewProject_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AddProjectWindow();
            if (dialog.ShowDialog() == true)
            {
                AddProject(dialog.ProjectName);
                projects.SaveProjects();
                LoadProject();
            }
        }

        private void RegisterDataFolder_Click(object sender, RoutedEventArgs e)
        {
            ShowRegisterDataView();
        }

        private void ShowRegisterDataView()
        {
            var folderDialog = new CommonOpenFileDialog("Select data folder");
            folderDialog.IsFolderPicker = true;
            if (folderDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                var optionDialog = new RegisterFilesWindow();
                optionDialog.SaveOptions();
                if (optionDialog.ShowDialog() == true)
                {
                    if (optionDialog.AsBook)
                    {
                        // !!!
                    }
                    else
                    {
                        var extensions = new List<string>();
                        if (optionDialog.Movie)
                        {
                            extensions.AddRange(new List<string> { ".mp4", ".webm", ".flv", ".wmv", ".mpg", ".mpeg", ".avi", ".mkv", ".dcv" });
                        }
                        if (optionDialog.Image)
                        {
                            extensions.AddRange(new List<string> { ".png", ".jpg", ".jpeg", ".bmp", ".gif" });
                        }

                        targetFiles.AddTargetFiles(projects.TargetFilesFile, folderDialog.FileName, extensions, optionDialog.Recursive);
                    }
                }
            }
        }

        private void ApplicationQuit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void ShowFolderFiles_Click(object sender, RoutedEventArgs e)
        {
            ShowFolderPathResult(ResultListView.SelectedItems);
            ShowFolderPathResult(ResultIconsView.SelectedItems);
        }

        private void ShowFolderPathResult(System.Collections.IList items)
        {
            foreach (TargetFile item in items)
            {
                var keyword = System.IO.Path.GetDirectoryName(item.Path.Substring(targetFiles.CommonPathLength));
                // !!! 選択されたファイルのフォルダをツリーで Expand する。
                // !!! FolderTreeView.SelectFolder(keyword);
                QueryTextBox.Text = keyword;
                break;
            }
        }

        private void RetrieveThumbnailItem_Click(object sender, RoutedEventArgs e)
        {
            RetrieveThumbnail(ResultListView.SelectedItems);
            RetrieveThumbnail(ResultIconsView.SelectedItems);
        }

        private void RetrieveThumbnail(System.Collections.IList items)
        {
            var indexes = new List<int>();

            foreach (TargetFile item in items)
            {
                var path = item.Path;
                var image = ThumbnailManager.LoadThumbnail(path, LongTimeout);
                ThumbnailManager.SaveThumbnail(path, image);

                int index = ResultListView.Items.IndexOf(item);
                indexes.Add(index);
            }

            foreach (int index in indexes)
            {
                var data = new TargetFile(resultData[index]);
                resultData[index] = data;
            }
        }

        private void RemoveItem_Click(object sender, RoutedEventArgs e)
        {
            RemoveFile(ResultListView.SelectedItems);
            RemoveFile(ResultIconsView.SelectedItems);

            ReshowResult();
        }

        private void RemoveFile(System.Collections.IList items)
        {
            foreach (TargetFile item in items)
            {
                targetFiles.RemoveFile(projects.ProjectFolder, item.Path);
            }
        }

        private void RoutedCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            TagEditView.FocusTagInput();
        }

        private void TilesButton_Click(object sender, RoutedEventArgs e)
        {
            SetIconsViewVisible(true);
        }

        private void ListButton_Click(object sender, RoutedEventArgs e)
        {
            SetIconsViewVisible(false);
        }

        private void SetIconsViewVisible(bool visible)
        {
            ResultIconsView.Visibility = visible ? Visibility.Visible : Visibility.Collapsed;
            ResultListView.Height = visible ? 0 : Double.NaN;
        }

        private void ProjectComboBox_DropDownClosed(object sender, EventArgs e)
        {
            resultData.Clear();
            var projectName = ProjectComboBox.SelectedItem as string;
            projects.ProjectName = projectName;
            SetDefaultProject(projectName);
            LoadProject();
        }

        private void SetDefaultProject(string name)
        {
            Properties.Settings.Default.Project = name;
        }

        private void AboutApplication_Click(object sender, RoutedEventArgs e)
        {
            // !!!
        }

        private void GridSplitter_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            Properties.Settings.Default.SplitterPosition = TreeViewWidth.Width.Value;
        }
    }
}
