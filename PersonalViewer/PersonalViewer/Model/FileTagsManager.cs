﻿using PersonalViewer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer.Model
{
    public class FileTagsManager
    {
        private Dictionary<string, HashSet<string>> taggedFiles = new Dictionary<string, HashSet<string>>();
        private TargetFilesManager filesManager;


        public void LoadData(string folderPath, TargetFilesManager targetFilesManager)
        {
            taggedFiles.Clear();

            filesManager = targetFilesManager;
            Tags = new HashSet<string>();

            foreach(var path in Directory.GetFiles(folderPath, "*.txt"))
            {
                var tag = Path.GetFileNameWithoutExtension(path);
                var files = new HashSet<string>(File.ReadLines(path, Encoding.UTF8));
                taggedFiles.Add(tag, files);
                if (files.Count > 0)
                {
                    Tags.Add(tag);
                }

                foreach (var file in files)
                {
                    filesManager.AddTag(file, tag);
                }
            }
        }

        public HashSet<string> Tags { get; private set; }

        public HashSet<string> Result(string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword))
            {
                return new HashSet<string>();
            }

            var queryPath = keyword.ToLower();
            var matchedTags = Tags.Where(delegate (string s) { return s.ToLower().Contains(queryPath); });

            var files = new HashSet<string>();
            foreach (var tag in matchedTags)
            {
                files.UnionWith(taggedFiles[tag]);
            }
            return files;
        }

        private string TagFile(string folderPath, string tag)
        {
            return Path.Combine(folderPath, tag + ".txt");
        }

        public void AddTag(string folderPath, string tag, HashSet<string> files)
        {
            if (string.IsNullOrWhiteSpace(tag) || (files.Count == 0))
            {
                return;
            }

            if (taggedFiles.ContainsKey(tag))
            {
                files.ExceptWith(taggedFiles[tag]);
                taggedFiles[tag].UnionWith(files);
            }
            else
            {
                taggedFiles.Add(tag, files);
                Tags.Add(tag);
            }
            File.AppendAllLines(TagFile(folderPath, tag), files);
            filesManager.AddTag(files, tag);
        }

        public void RemoveTag(string folderPath, string tag, HashSet<string> files)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                return;
            }

            if (!taggedFiles.ContainsKey(tag))
            {
                return;
            }

            taggedFiles[tag].RemoveWhere(s => files.Contains(s));
            File.WriteAllLines(TagFile(folderPath, tag), taggedFiles[tag]);
            filesManager.RemoveTag(files, tag);
        }

        public void RemoveNotExistsFiles(string folder, string[] removedFiles)
        {
            foreach (var tagName in new List<string>(taggedFiles.Keys))
            {
                var diff = new HashSet<string>(taggedFiles[tagName].Except(removedFiles));
                if (diff.Count != taggedFiles[tagName].Count)
                {
                    taggedFiles[tagName] = diff;
                    File.WriteAllLines(TagFile(folder, tagName), diff);
                }
            }
        }
    }
}
