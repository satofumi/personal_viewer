﻿using PersonalViewer.Control;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer.Model
{
    public class PlayerSettings
    {
        private class YamlPlayers
        {
            public class Player
            {
                public string Path { get; set; }
                public List<string> Ext { get; set; }
                public string Options { get; set; }
            }

            public List<Player> Players { get; set; }
        }

        private class YamlPlayerFiles
        {
            // !!!
        }


        private List<string> defaultProgramFiles;


        public void LoadData(string folder)
        {
            var playersPath = Path.Combine(folder, "players.yaml");
            if (File.Exists(playersPath))
            {
                var yamlPlayers = YamlImporter.Deserialize<YamlPlayers>(playersPath);
                // !!!
            }

            var defaultProgramPath = Path.Combine(folder, "default_program.txt");
            defaultProgramFiles = File.Exists(defaultProgramPath) ? File.ReadAllLines(defaultProgramPath).ToList() : new List<string>();
        }

        public bool IsDefaultProgram(string path)
        {
            return defaultProgramFiles.Contains(path);
        }
    }
}
