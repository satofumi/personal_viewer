﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace PersonalViewer.Model
{
    public class YamlImporter
    {
        public static T Deserialize<T>(string path)
        {
            using (var input = File.OpenText(path))
            {
                var deserializer = new DeserializerBuilder().WithNamingConvention(new UnderscoredNamingConvention()).Build();
                return deserializer.Deserialize<T>(input);
            }
        }
    }
}
