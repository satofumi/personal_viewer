﻿using Microsoft.WindowsAPICodePack.Shell;
using PersonalViewer.Control;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PersonalViewer.Model
{
    public class ThumbnailManager : IValueConverter
    {
        public static Dictionary<string, BitmapSource> thumbnails = new Dictionary<string, BitmapSource>();

        private const int Timeout = 500;
        private static bool initialized = false;
        private static Dictionary<string, string> nameToImage = new Dictionary<string, string>();
        private static BitmapSource emptyImage;
        private static string thumbnailFolderPath;


        public static void LoadData(string folderPath)
        {
            thumbnailFolderPath = folderPath;
            thumbnails.Clear();

            var thumbnailsFile = Path.Combine(folderPath, "thumbnails.txt");
            if (!File.Exists(thumbnailsFile))
            {
                return;
            }

            using (var stream = File.OpenRead(thumbnailsFile))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8, true))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var tokens = line.Split('\t');
                        if ((tokens.Length != 2) || string.IsNullOrWhiteSpace(tokens[1]))
                        {
                            continue;
                        }

                        var path = tokens[0];
                        var imagePath = tokens[1];
                        nameToImage[path] = imagePath;
                    }
                }
            }

#if false
            // 例外が発生するのでコメントアウトする。

            // 利用していないサムネイル画像を削除する。
            var existFiles = Directory.GetFiles(folderPath, "*.bmp").ToList();
            foreach (var item in nameToImage)
            {
                var usingFile = item.Value;
                existFiles.Remove(usingFile);
            }
            foreach (var file in existFiles)
            {
                File.Delete(file);
            }
#endif
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!initialized)
            {
                emptyImage = BitmapSource.Create(4, 3, 1, 1, PixelFormats.BlackWhite, null, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 1);
                initialized = true;
            }

            if (value == null)
            {
                return null;
            }

            var path = value.ToString();

            if (thumbnails.ContainsKey(path))
            {
                return thumbnails[path];
            }
            else if (nameToImage.ContainsKey(path))
            {
                var imagePath = nameToImage[path];
                if (File.Exists(imagePath))
                {
                    thumbnails[path] = new BitmapImage(new Uri(imagePath));
                    return thumbnails[path];
                }
            }

            var image = LoadThumbnail(path, Timeout);
            if (image != emptyImage)
            {
                SaveThumbnail(path, image);
            }
            return image;
        }

        public static BitmapSource LoadThumbnail(string path, int timeout)
        {
            var task = Task.Run(() => LoadThumbnail(path));
            if (task.Wait(TimeSpan.FromMilliseconds(timeout)))
            {
                lock (thumbnails)
                {
                    thumbnails[path] = task.Result;
                }
                return task.Result;
            }
            else
            {
                var thumbnailLine = path + "\t\r";
                lock (thumbnails)
                {
                    File.AppendAllLines(Path.Combine(thumbnailFolderPath, Constant.ThumbnailsFile), new List<string>() { thumbnailLine });
                    nameToImage[path] = "";
                }
                return emptyImage;
            }
        }

        private static BitmapSource LoadThumbnail(string path)
        {
            try
            {
                var shellFile = ShellFile.FromFilePath(path);
                var thumb = shellFile.Thumbnail.MediumBitmap;
                var image = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(thumb.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                image.Freeze();
                return image;
            }
            catch (Exception)
            {
                // ファイルが存在しないときに例外が発生する。
                var projectTopFolder = Path.GetDirectoryName(Path.GetDirectoryName(path));
                File.AppendAllLines(Path.Combine(projectTopFolder, Constant.RemovedFilesFile), new List<string>() { path });
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public static void SaveThumbnail(string path, BitmapSource bitmap)
        {
            var thumbnailPath = Path.Combine(thumbnailFolderPath, Path.GetRandomFileName() + ".bmp");
            using (var stream = new FileStream(thumbnailPath, FileMode.Create))
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }

            lock (thumbnails)
            {
                lock (thumbnails)
                {
                    nameToImage[path] = thumbnailPath;
                }
            }

            var lines = new List<string>();
            lock (thumbnails)
            {
                foreach (var item in nameToImage)
                {
                    var value = string.IsNullOrWhiteSpace(item.Value) ? "" : item.Value;
                    lines.Add(item.Key + '\t' + value);
                }
            }

            lock (thumbnails)
            {
                File.WriteAllLines(Path.Combine(thumbnailFolderPath, Constant.ThumbnailsFile), lines);
            }
        }
    }
}
