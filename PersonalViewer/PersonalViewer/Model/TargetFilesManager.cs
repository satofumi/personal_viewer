﻿using PersonalViewer.Control;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer.Model
{
    public class TargetFilesManager
    {
        private List<string> files = new List<string>();
        private Dictionary<string, HashSet<string>> tags = new Dictionary<string, HashSet<string>>();


        public int CommonPathLength { get; private set; }

        public void LoadData(string dbFile)
        {
            files.Clear();
            if (!File.Exists(dbFile))
            {
                return;
            }

            using (var stream = File.OpenRead(dbFile))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8, true))
                {
                    CommonPathLength = int.MaxValue;
                    string commonPath = null;

                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }

                        if (commonPath == null)
                        {
                            commonPath = line;
                            CommonPathLength = line.Length;
                        }
                        else
                        {
                            for (int i = Math.Min(CommonPathLength, line.Length); i >= 0; --i)
                            {
                                if (commonPath.Substring(0, i) == line.Substring(0, i))
                                {
                                    CommonPathLength = i;
                                    break;
                                }
                            }
                        }

                        files.Add(line);
                    }
                }
            }

            // 共通パスの長さの位置がフォルダの区切りでないときには、フォルダの区切りの長さにする。
            if (files.Count > 0)
            {
                var path = files[0];
                var folderPath = "";
                int length = 0;

                if (path[CommonPathLength] != Path.DirectorySeparatorChar)
                {
                    var folders = path.Split(Path.DirectorySeparatorChar);

                    for (int i = 0; i < folders.Length; ++i)
                    {
                        folderPath = Path.Combine(folderPath, folders[i]);
                        if (folderPath.Length < CommonPathLength)
                        {
                            length = folderPath.Length + 1;
                        }
                    }
                    CommonPathLength = length;
                }
            }
        }

        public List<string> Files
        {
            get
            {
                return files;
            }
        }

        public List<string> FolderResult(string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword))
            {
                return new List<string>();
            }

            var queryPath = keyword.ToLower();
            System.Diagnostics.Debug.WriteLine(Path.GetDirectoryName(files[0].Substring(CommonPathLength).ToLower()));
            return files.FindAll(delegate (string s) { return Path.GetDirectoryName(s.Substring(CommonPathLength).ToLower()) == queryPath; });
        }

        public List<string> Result(string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword))
            {
                return new List<string>();
            }

            var queryPath = keyword.ToLower();
            return files.FindAll(delegate (string s) { return s.Substring(CommonPathLength).ToLower().Contains(queryPath); });
        }

        public HashSet<string> Tags(string path)
        {
            if (!tags.ContainsKey(path))
            {
                return new HashSet<string>();
            }
            else
            {
                return tags[path];
            }
        }

        public void AddTargetFiles(string dbFile, string folderPath, List<string> extensions, bool recursive)
        {
            var appendFiles = new List<string>();
            foreach (var path in Directory.GetFiles(folderPath, "*.*", SearchOption.AllDirectories).Where(f => extensions.IndexOf(Path.GetExtension(f).ToLower()) >= 0))
            {
                if (!files.Contains(path))
                {
                    appendFiles.Add(path);
                    files.Add(path);
                }
            }
            File.AppendAllLines(dbFile, appendFiles);
        }

        public void RemoveFile(string projectName, string path)
        {
            File.AppendAllLines(Path.Combine(projectName, Constant.RemovedFilesFile), new List<string>() { path });
            files.Remove(path);
        }

        public void AddTag(string path, string tag)
        {
            if (tags.ContainsKey(path))
            {
                tags[path].Add(tag);
            }
            else
            {
                tags.Add(path, new HashSet<string>() { tag });
            }
        }

        public void AddTag(HashSet<string> pathes, string tag)
        {
            foreach (var path in pathes)
            {
                AddTag(path, tag);
            }
        }

        public void RemoveTag(string path, string tag)
        {
            // !!!
        }

        public void RemoveTag(HashSet<string> pathes, string tag)
        {
            foreach (var path in pathes)
            {
                RemoveTag(path, tag);
            }
        }

        public void RemoveNotExistsFiles(string dbFile, string[] removedFiles)
        {
            var backupFile = dbFile + Constant.BackupExtension;
            File.Delete(backupFile);
            File.Copy(dbFile, backupFile);

            var updatedFiles = files.Except(removedFiles);
            File.Delete(dbFile);
            File.AppendAllLines(dbFile, updatedFiles);
            files = updatedFiles.ToList();

            var path = Path.Combine(Path.GetDirectoryName(dbFile), Constant.RemovedFilesFile);
            File.Delete(path);
            File.Delete(backupFile);
        }
    }
}
