﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PersonalViewer.Model
{
    public class TargetFile
    {
        public string Path { get; set; }
        public string FileName { get; set; }
        public string Thumbnail { get; set; }
        public string Tags { get; set; }

        public TargetFile(string path)
        {
            Path = path;
            FileName = System.IO.Path.GetFileNameWithoutExtension(path);
            Thumbnail = path;
        }

        public TargetFile(TargetFile other)
        {
            Path = other.Path;
            FileName = other.FileName;
            Thumbnail = other.Thumbnail;
            Tags = other.Tags;
        }
    }
}
