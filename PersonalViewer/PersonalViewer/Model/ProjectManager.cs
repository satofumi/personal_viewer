﻿using PersonalViewer.Control;
using PersonalViewer.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer.Model
{
    class ProjectManager
    {
        public string ProjectName { get; set; }
        public HashSet<string> ProjectNames { get; private set; }


        public void LoadProjects(string path)
        {
            try
            {
                ProjectNames = new HashSet<string>(File.ReadAllLines(Constant.ProjectFilesFile));
            }
            catch (Exception)
            {
                ProjectNames = new HashSet<string>() { Constant.DefaultProjectName };
            }
        }

        public void SaveProjects()
        {
            File.WriteAllLines(Constant.ProjectFilesFile, ProjectNames);
        }

        public string TargetFilesFile
        {
            get
            {
                return Path.Combine(Constant.DataFolder, ProjectName, Constant.TargetFilesFile);
            }
        }
        public string TagsFolder
        {
            get
            {
                return Path.Combine(Constant.DataFolder, ProjectName, Constant.FileTagsFolder);
            }
        }

        public string BooksFile
        {
            get
            {
                return Path.Combine(Constant.DataFolder, ProjectName, Constant.BooksFile);
            }
        }

        public void CreateFolders()
        {
            var projectFolder = Path.Combine(Constant.DataFolder, ProjectName);
            Directory.CreateDirectory(Path.Combine(projectFolder, Constant.ThumbnailsFolder));
            Directory.CreateDirectory(Path.Combine(projectFolder, Constant.FileTagsFolder));
            Directory.CreateDirectory(Path.Combine(projectFolder, Constant.BooksFolder));
            Directory.CreateDirectory(Path.Combine(projectFolder, Constant.BookTagsFolder));
            Directory.CreateDirectory(Path.Combine(projectFolder, Constant.PlayerSettingFolder));
        }

        public string ProjectFolder
        {
            get
            {
                return Path.Combine(Constant.DataFolder, ProjectName);
            }
        }

        public void LoadData(TargetFilesManager targetFiles, FolderTreeView folderTreeView, FileTagsManager fileTags, BooksManager books, BookTagsManager bookTags, PlayerSettings playerSettings)
        {
            var projectFolder = ProjectFolder;

            targetFiles.LoadData(TargetFilesFile);
            folderTreeView.ParsePath(targetFiles.Files, targetFiles.CommonPathLength);
            var fileTagsFolder = Path.Combine(projectFolder, Constant.FileTagsFolder);
            fileTags.LoadData(fileTagsFolder, targetFiles);

            books.LoadData(BooksFile);
            var bookTagsFolder = Path.Combine(projectFolder, Constant.BookTagsFolder);
            bookTags.LoadData(bookTagsFolder, books);

            if (File.Exists(Path.Combine(projectFolder, Constant.RemovedFilesFile)))
            {
                // 削除すべきファイル情報があれば、既存のファイルリストを更新する。
                var removedFiles = File.ReadAllLines(Path.Combine(projectFolder, Constant.RemovedFilesFile));
                targetFiles.RemoveNotExistsFiles(TargetFilesFile, removedFiles);
                fileTags.RemoveNotExistsFiles(fileTagsFolder, removedFiles);
            }
            ThumbnailManager.LoadData(Path.Combine(projectFolder, Constant.ThumbnailsFolder));
            playerSettings.LoadData(Path.Combine(projectFolder, Constant.PlayerSettingFolder));
        }
    }
}
