﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer.Model
{
    public class BooksManager
    {
        public void LoadData(string dbFile)
        {
            // !!!
        }

        public List<string> Result(string keyword)
        {
            // !!!
            return new List<string>();
        }

        public void AddBookFolders(string dbFile, string folderPath, List<string> extensions, bool recursive)
        {
            // !!!
        }

        public void RemoveFolder(string projectName, string folderPath)
        {
            // !!!
        }

        public void AddTag(string folderPath, string tag)
        {
            // !!!
        }

        public void AddTag(HashSet<string> folderPathes, string tag)
        {
            foreach (var path in folderPathes)
            {
                AddTag(path, tag);
            }
        }

        public void RemoveTag(string folderPath, string tag)
        {
            // !!!
        }

        public void RemoveTag(HashSet<string> folderPathes, string tag)
        {
            foreach (var path in folderPathes)
            {
                RemoveTag(path, tag);
            }
        }

        public void RemoveNotExistsFolders(string dbFile, string[] removedFolders)
        {
#if false
            var backupFile = dbFile + Constant.BackupExtension;
            File.Delete(backupFile);
            File.Copy(dbFile, backupFile);

            var updatedFiles = files.Except(removedFiles);
            File.Delete(dbFile);
            File.AppendAllLines(dbFile, updatedFiles);
            files = updatedFiles.ToList();

            var path = Path.Combine(Path.GetDirectoryName(dbFile), Constant.RemovedFilesFile);
            File.Delete(path);
            File.Delete(backupFile);
#endif
        }
    }
}
