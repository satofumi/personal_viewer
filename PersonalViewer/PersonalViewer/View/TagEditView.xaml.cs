﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersonalViewer.View
{
    /// <summary>
    /// TagEditView.xaml の相互作用ロジック
    /// </summary>
    public partial class TagEditView : UserControl
    {
        public class AddTagEventArgs : EventArgs
        {
            public string Tag { get; set; }

            public AddTagEventArgs(string tag)
            {
                Tag = tag;
            }
        }
        public event EventHandler<AddTagEventArgs> AddTag;


        public TagEditView()
        {
            InitializeComponent();
        }

        private void AddTagTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                AddTag?.Invoke(this, new AddTagEventArgs(AddTagTextBox.Text));
                AddTagTextBox.Text = "";
            }
        }

        public void FocusTagInput()
        {
            AddTagTextBox.Focus();
        }
    }
}
