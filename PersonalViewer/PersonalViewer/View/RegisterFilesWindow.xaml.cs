﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PersonalViewer.View
{
    /// <summary>
    /// RegisterFilesWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class RegisterFilesWindow : Window
    {
        public RegisterFilesWindow()
        {
            InitializeComponent();

            LoadOptions();
        }

        private void LoadOptions()
        {
            Recursive = Properties.Settings.Default.RegisterRecursive;
            AsBook = Properties.Settings.Default.RegisterAsBook;
            Movie = Properties.Settings.Default.RegisterFileTypeMovie;
            Image = Properties.Settings.Default.RegisterFileTypeImage;
        }

        public void SaveOptions()
        {
            Properties.Settings.Default.RegisterRecursive = Recursive;
            Properties.Settings.Default.RegisterAsBook = AsBook;
            Properties.Settings.Default.RegisterFileTypeMovie = Movie;
            Properties.Settings.Default.RegisterFileTypeImage = Image;
            Properties.Settings.Default.Save();
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public bool Recursive
        {
            get
            {
                var isChecked = RecursiveCheckBox.IsChecked;
                return isChecked.HasValue && isChecked.Value;
            }
            set
            {
                RecursiveCheckBox.IsChecked = value;
            }
        }

        public bool AsBook
        {
            get
            {
                var isChecked = AsBookCheckBox.IsChecked;
                return isChecked.HasValue && isChecked.Value;
            }
            set
            {
                AsBookCheckBox.IsChecked = value;
            }
        }

        public bool Movie
        {
            get
            {
                var isChecked = MovieCheckBox.IsChecked;
                return isChecked.HasValue && isChecked.Value;
            }
            set
            {
                MovieCheckBox.IsChecked = value;
            }
        }

        public bool Image
        {
            get
            {
                var isChecked = ImageCheckBox.IsChecked;
                return isChecked.HasValue && isChecked.Value;
            }
            set
            {
                ImageCheckBox.IsChecked = value;
            }
        }
    }
}
