﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PersonalViewer.View
{
    /// <summary>
    /// AddNewProject.xaml の相互作用ロジック
    /// </summary>
    public partial class AddProjectWindow : Window
    {
        public string ProjectName
        {
            get
            {
                return ProjectTextBox.Text;
            }
        }

        public AddProjectWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ProjectTextBox.Focus();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AddProject();
        }

        private void AddProject()
        {
            DialogResult = true;
        }

        private void ProjectTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AddProject();
            }
        }
    }
}
