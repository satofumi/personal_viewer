﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersonalViewer.View
{
    /// <summary>
    /// FolderTreeView.xaml の相互作用ロジック
    /// </summary>
    public partial class FolderTreeView : UserControl
    {
        public class SelectedFolderEventArgs : EventArgs
        {
            public string FolderPath { get; set; }

            public SelectedFolderEventArgs(string folderPath)
            {
                FolderPath = folderPath;
            }
        }

        public event EventHandler<SelectedFolderEventArgs> FolderSelected;


        private class FolderNode
        {
            private FolderNode Parent { get; set; }


            public string Name { get; set; }
            public List<FolderNode> Children { get; set; }
            public string FolderPath { get; set; }


            public FolderNode()
            {
                Children = new List<FolderNode>();
            }

            public void AddChild(FolderNode node)
            {
                Children.Add(node);
            }
        }

        private FolderNode Top;
        private Dictionary<string, FolderNode> folderIndex = new Dictionary<string, FolderNode>();


        public FolderTreeView()
        {
            InitializeComponent();
        }

        public void ParsePath(List<string> files, int commonPathLength)
        {
            if (files.Count == 0)
            {
                return;
            }

            Top = new FolderNode();
            var commonPath = files[0].Substring(0, commonPathLength);

            foreach (var path in files)
            {
                var parent = Top;

                // フォルダに対して処理を行う。
                var folders = path.Substring(commonPathLength).Split(System.IO.Path.DirectorySeparatorChar);
                var folderPath = "";

                // 最後のトークンはファイルなので処理を行わない。
                for (int i = 0; i < folders.Length - 1; ++i)
                {
                    var folder = folders[i];
                    folderPath = System.IO.Path.Combine(folderPath, folder);
                    if (folderIndex.ContainsKey(folderPath))
                    {
                        parent = folderIndex[folderPath];
                        continue;
                    }

                    var node = new FolderNode();
                    node.Name = folder;
                    node.FolderPath = folderPath;

                    parent.AddChild(node);
                    folderIndex.Add(folderPath, node);

                    parent = node;
                }
            }

            Folders.ItemsSource = Top.Children;
        }

        private void Folders_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var item = e.NewValue as FolderNode;
            if (item != null)
            {
                FolderSelected?.Invoke(this, new SelectedFolderEventArgs(item.FolderPath));
            }
        }
    }
}
