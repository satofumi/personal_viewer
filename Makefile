DEVENV = /c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2017/Community/Common7/IDE/devenv.exe

VERSION = `cat ChangeLog.txt | awk "NR==2,NR==2 {print}" | awk '$$0 = substr($$0, 5)'`
RELEASE_NAME = PersonalViewer-$(VERSION)


all :

clean :

.PHONY : all clean dist

dist :
	python generate_info_class.py > PersonalViewer/PersonalViewer/VersionInformation.cs
	$(DEVENV) PersonalViewer/installer/installer.vdproj -rebuild "Installer|Any CPU" -out $(PWD)/build_log.txt
	$(RM) -rf $(PACKAGE_DIR)
	cp PersonalViewer/installer/Installer/installer.msi $(RELEASE_NAME).msi
