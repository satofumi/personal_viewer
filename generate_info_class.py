import subprocess

version = '0.0.0'
with open('ChangeLog.txt') as f:
    f.readline()
    line = f.readline().strip()
    version = line[2:]

revision = subprocess.check_output(['hg', 'id', '-i']).strip()


text = '''using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalViewer
{
    public class VersionInformation
    {
        public static string VersionText = "%s";
        public static string RevisionText = "%s";
    }
}
'''

print(text % (version, revision))
